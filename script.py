#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import logging, sys
from Crypto.Cipher import AES, DES

def padKeyAes(key):
	paddedKey = pad(key.encode(),'AES').decode()
	if(len(paddedKey) not in [16,24,32]):
		logging.warning("Key too long. Padding key with size: "+str(len(paddedKey)))
		if(len(paddedKey)>32):	return paddedKey[:32]
		if(len(paddedKey)>24):	return paddedKey[:24]
		if(len(paddedKey)>16):	return paddedKey[:16]
	return pad(key.encode(),'AES').decode()

def padKeyDes(key):
	if(len(key)<8):		return pad(key.encode(),'DES').decode()[:8]
	if(len(key)>8):		return pad(key[:8].encode(),'DES').decode()
	return pad(key.encode(),'DES').decode()

def padIV(iv,mode):
	padded_IV = pad(iv.encode(),mode).decode()
	if(mode=='AES' and len(padded_IV)>AES.block_size):	return padded_IV[:AES.block_size]
	if(mode=='DES' and len(padded_IV)>DES.block_size):	return padded_IV[:DES.block_size]
	return padded_IV

def pad(data,mode):
	if(mode=='AES'):	length	=	(AES.block_size - len(data)) % AES.block_size
	elif(mode=='DES'):	length	=	(DES.block_size - len(data)) % DES.block_size
	return	data + bytes([length])*length

def unpad(data):	return data[:-data[-1]]

def toUtf8(data):	return data.encode('UTF-8')
####	DES

def desCipher(key,data,operation,mode,iv=None):
	if(operation=='ECB'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_ECB)
	elif(operation=='CBC'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_CBC, toUtf8(padIV(iv,'DES')))
	elif(operation=='OFB'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_OFB, toUtf8(padIV(iv,'DES')))
	elif(operation=='CFB'):		tempCipher = DES.new(toUtf8(padKeyDes(key)), DES.MODE_CFB, toUtf8(padIV(iv,'DES')))
	return tempCipher.encrypt(pad(data,'DES'))	if (mode=='ENCRYPT') else  tempCipher.decrypt(pad(data,'DES')) 

####	AES

def aesCipher(key,data,operation,mode,iv=None):
	if(operation=='ECB'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_ECB)
	elif(operation=='CBC'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_CBC, toUtf8(padIV(iv,'AES')))
	elif(operation=='OFB'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_OFB, toUtf8(padIV(iv,'AES')))
	elif(operation=='CFB'):		tempCipher = AES.new(toUtf8(padKeyAes(key)), AES.MODE_CFB, toUtf8(padIV(iv,'AES')))
	return tempCipher.encrypt(pad(data,'AES'))	if (mode=='ENCRYPT') else  tempCipher.decrypt(pad(data,'AES')) 

if __name__ == "__main__":
	
	if(len(sys.argv)<5):	sys.exit(-1)
#	logging.basicConfig(level=logging.INFO)
	args = [arg.strip() for arg in sys.argv]				
	input_file   = args[1]
	output_file  = args[2]
	key 		 = args[3]
	iv 			 = args[4]
	aes_or_des 	 = args[5]
	enc_or_dec	 = args[6]
	print("key:",key)
	print("iv:",iv)
	print("aes/des (1/0)",aes_or_des)
	print("enc/dec (1/0)",enc_or_dec)

	#
	#	./script INPUTFILE OUTPUTFILE KEY IV 1  1  <= Cifrar con AES
	#	./script INPUTFILE OUTPUTFILE KEY IV 1  0  <= descifrar con AES
	#	./script INPUTFILE OUTPUTFILE KEY IV 0  1  <= Cifrar con DES
	#	./script INPUTFILE OUTPUTFILE KEY IV 0  0  <= descifrar con DES
	#
	#										/\  /\___ enc_or_dec
	#										|
	#									aes_or_des
	

	IV 	= padIV(iv,'AES')	 if 	aes_or_des else padIV(iv,'DES')
	KEY =  padKeyAes(key)	 if 	aes_or_des else padKeyDes(key)
	try:
		with open(output_file,'wb') as output_descriptor:
			with open(input_file,'rb') as input_descriptor:
				if aes_or_des=='1':		
					if enc_or_dec=='1':		output_descriptor.write(aesCipher(KEY,input_descriptor.read(),'CFB','ENCRYPT',IV))
					else:					output_descriptor.write(unpad(aesCipher(KEY,input_descriptor.read(),'CFB','DECRYPT',IV)))
				else:
					if enc_or_dec=='1':		output_descriptor.write(desCipher(KEY,input_descriptor.read(),'CFB','ENCRYPT',IV))
					else:				output_descriptor.write(unpad(desCipher(KEY,input_descriptor.read(),'CFB','DECRYPT',IV)))
		sys.exit(0)
	except Exception as e:
		print("There's been an error! ",e)
		sys.exit(-1)