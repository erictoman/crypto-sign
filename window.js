var path = require('path');
var $ = require('jquery')
var keypath = ""
var privatekeypath = ""
var fs = require("fs")
var filepath = ""
var destpath = ""
var modules = require("./modules")
var {
    dialog
} = require("electron").remote
var bandera = 0

function includes(path) {
    return new Promise((resolve) => {
        if (path.includes("AES")) {
            resolve("_AES_", "_DEC_")
        } else {
            resolve("_DES_", "_DEC_")
        }
    })
}

$(() => {
    $("#validate").click(() => {
        if (bandera) {
            openkey().then((key) => {
                var key = key.split("|")
                var iv = key[1]
                key = key[0]
                alert("Public sender key")
                openFile().then((secondpath) => {
                    alert("Recieved file")
                    openFile().then((thirdpath) => {
                        console.log("TH", thirdpath)
                        includes(thirdpath).then((opt, action) => {
                            enc_decPromise(opt, action, thirdpath, thirdpath + ".temp.txt", iv, key).then((res) => {
                                if (res) {
                                    setTimeout(() => {
                                        var resultado = modules.SignVerify(secondpath, thirdpath + ".temp.txt")
                                        if (resultado) {
                                            alert("Signature OK!")
                                        } else {
                                            alert("File has been modified!")
                                        }
                                        fs.unlinkSync(thirdpath+ ".temp.txt")
                                    }, 1000);
                                } else {
                                    alert("Error decrypting file")
                                }
                            })
                        })
                    })
                })
            })
        } else {
            openkey().then((key) => {
                var key = key.split("|")
                var iv = key[1]
                key = key[0]
                alert("Recieved file")
                openFile().then((thirdpath) => {
                    console.log("TH", thirdpath)
                    includes(thirdpath).then((opt, action) => {
                        enc_decPromise(opt, action, thirdpath, thirdpath + ".temp.txt", iv, key).then((res) => {
                            if (res) {
                                setTimeout(() => {
                                    var resultado = modules.shaVerify(thirdpath + ".temp.txt")
                                    if (resultado) {
                                        alert("Signature OK!")
                                    } else {
                                        alert("File has been modified!")
                                    }
                                    fs.unlinkSync(thirdpath+ ".temp.txt")
                                }, 1000);
                            } else {
                                alert("Error decrypting file")
                            }
                        })
                    })
                })
            })
        }
    })
    $("#joinHASH").click(function () {
        openFile(true)
    })
    $("#separeHASH").click(function () {
        openFile(false)
    })
    $("#back").click(() => {
        $("#modal").removeClass("is-active")
    })
    $("#OP1,#OP2,#OP3").on('change', () => {
        allHiden()
        var OP1 = $("#OP1").prop('checked')
        var OP2 = $("#OP2").prop('checked')
        var OP3 = $("#OP3").prop('checked')
        if (OP1 && !OP2 && !OP3) {
            $("#secAES").show()
            $("#CONFINTAES-OPT").prop("checked",false)
            $("#CONFINTAES-OPT").prop("disabled",false)
        } else {
            if (OP3 && !OP2 && !OP1) {
                $("#secAuth").show()
            } else {
                if (!OP3 && OP2 && !OP1) {
                    $("#allProcess").show()
                } else {
                    if (OP3 && !OP2 && OP1) {
                        $("#CONFINT").show()
                        $("#authprivate").hide()
                        $("#title").text("Confidentiality & Integrity")
                        $("#CONFINTAES-OPT").prop("checked",true)
                        $("#CONFINTAES-OPT").prop("disabled",true)
                        bandera = 0
                    } else {
                        if (OP2 && OP1 && !OP3) {
                            $("#CONFINT").show()
                            $("#authprivate").show()
                            $("#title").text("Confidentiality & Authentication")
                            $("#CONFINTAES-OPT").prop("checked",true)
                            $("#CONFINTAES-OPT").prop("disabled",true)
                            bandera = 1
                        } else {
                            if (OP2 && OP1 && OP3) {
                                $("#CONFINT").show()
                                $("#authprivate").show()
                                $("#CONFINTAES-OPT").prop("checked",true)
                                $("#CONFINTAES-OPT").prop("disabled",true)
                                $("#title").text("Confidentiality,Authentication & Integrity")
                                bandera = 1
                            } else {
                                if (!OP1 && !OP2 && !OP3) {

                                } else {
                                    if (!OP1 && OP2 && OP3) {
                                        $("#allProcess").show()
                                    } else {
                                        alert("Not valid combination")
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    })
    //Confidencialidad e integridad
    $('#runCONFINT').click(() => {
        if (($("#CONFINTkey").val().length == 8 && !$("#CONFINTOPT-CIPHER").prop("checked")) || ($("#CONFINTkey").val().length >= 8 && $("#CONFINTOPT-CIPHER").prop("checked"))) {
            var set1 = $("#SetPublicKey").hasClass('is-danger')
            var set2 = $("#SetFile").hasClass('is-danger')
            var set3 = $("#SetDestFolder").hasClass('is-danger')
            if (!set1 && !set2 && !set3) {
                if ($("#CONFINTOPT-CIPHER").prop("checked")) {
                    algo = "_AES_"
                } else {
                    algo = "_DES_"
                }
                if ($("#CONFINTAES-OPT").prop("checked")) {
                    tipo = "_ENC_"
                } else {
                    tipo = "_DES_"
                }
                if (bandera == 0) {
                    var hashedfile = saveFilepath(filepath, "_WITH_HASH_")
                    modules.shaFile(filepath, hashedfile)
                } else {
                    var hashedfile = saveFilepath(filepath, "_SIGNED_")
                    modules.SignatureFile(filepath, hashedfile, privatekeypath)
                }
                var finaldestpath = saveFilepath(hashedfile, algo + tipo)
                finaldestpath = destpath + "/" + path.basename(finaldestpath)
                iv = makeid(16)
                enc_decPromise(algo, tipo, hashedfile, finaldestpath, iv).then((res) => {
                    if (res) {
                        saveKey(keypath, $("#CONFINTkey").val(), destpath + "/key.txt", iv).then((res) => {
                            if (res) {
                                alert("Key saved")
                                setDanger()
                            }
                        })
                    }
                })
            }
        } else {
            alert("All filles have not been choosen or key not set")
        }
    })

    function enc_decPromise(algo, tipo, hashedfile, finaldestpath, iv, key = $("#CONFINTkey").val()) {
        return new Promise((resolve) => {
            setTimeout(() => {
                if (algo === "_AES_") {
                    if (tipo === "_ENC_") {
                        modules.AESCipher(hashedfile, finaldestpath, key, iv, 1, 1)
                        resolve(1)
                    } else {
                        modules.AESCipher(hashedfile, finaldestpath, key, iv, 1, 0)
                        resolve(1)
                    }
                } else {
                    if (tipo === "_ENC_") {
                        modules.AESCipher(hashedfile, finaldestpath, key, iv, 0, 1)
                        resolve(1)
                    } else {
                        modules.AESCipher(hashedfile, finaldestpath, key, iv, 0, 0)
                        resolve(1)
                    }
                }
            }, 2000)
        })
    }


    $("#SetPublicKey").click(() => {
        openFile().then((path) => {
            keypath = path
            $("#SetPublicKey").removeClass('is-danger')
            $("#SetPublicKey").addClass('is-success')
        })
    })

    $("#SetPrivateKey").click(() => {
        openFile().then((path) => {
            privatekeypath = path
            $("#SetPrivateKey").removeClass('is-danger')
            $("#SetPrivateKey").addClass('is-success')
        })
    })

    $("#SetFile").click(() => {
        openFile().then((path) => {
            filepath = path
            $("#SetFile").removeClass('is-danger')
            $("#SetFile").addClass('is-success')
        })
    })
    $("#SetDestFolder").click(() => {
        operFolder().then((path) => {
            destpath = path
            $("#SetDestFolder").removeClass('is-danger')
            $("#SetDestFolder").addClass('is-success')
        })
    })

    $("#AUTHButton").click(() => {
        openFile().then((filepath) => {
            if ($("#AUTHType").prop("checked")) {
                saveFile(filepath, "_WITH_HASH_").then((filedest) => {
                    modules.shaFile(filepath, filedest)
                })
            } else {
                modules.shaVerify(filepath).then((res) => {
                    if (res) {
                        alert("Validation OK!")
                    } else {
                        alert("Validation Failed! Hash don't match!")
                    }
                })
            }
        })
    })

    $("#AESFILE").click(() => {
        if (!$("#AES-OPT").prop("checked")) {
            openkey().then((key) => {
                key = key.split("|")
                $("#key").val(key[0])
                iv = key[1]
                if ($("#key").val().length >= 8) {
                    ENCDEC(iv).then((resultado) => {
                        if (resultado) {
                            alert("Decrypted!")
                        }
                    })
                } else {
                    alert("Key length not correct or invalid for encryption")
                }
            })
        } else {
            if (($("#key").val().length == 8 && !$("#OPT-CIPHER").prop("checked")) || ($("#key").val().length >= 8 && $("#OPT-CIPHER").prop("checked"))) {
                iv = makeid(16)
                ENCDEC(iv).then((resultado) => {
                    if (resultado) {
                        alert("Choose public key")
                        openFile().then((filepath) => {
                            saveFile('', 'key.txt').then((filedest) => {
                                saveKey(filepath, $("#key").val(), filedest, iv).then(() => {
                                    alert("Key saved")
                                })
                            })
                        })
                    }
                })
            } else {
                alert("Key length not correct or invalid for encryption")
            }
        }
    })

    $("#allProcessButton").click(() => {
        if ($("#VerifySignature").prop("checked")) {
            alert("Choose senders private key")
            openFile().then((filepath1) => {
                alert("Choose file")
                openFile().then((filepath2) => {
                    if ($("#VerifySignature").prop("checked")) {
                        tipo = "_SIGNED_"
                        saveFile(filepath2, tipo).then((filedest) => {
                            modules.SignatureFile(filepath2.toString(), filedest.toString(), filepath1.toString())
                            alert("File signed")
                        })
                    } else {

                    }
                })
            })
        } else {
            alert("Choose public key")
            openFile().then((keypath) => {
                alert("Choose file")
                openFile().then((filepath_s) => {
                    if (modules.SignVerify(keypath, filepath_s)) {
                        alert("Signature Validated!")
                    } else {
                        alert("Signature failed!")
                    }
                })
            })
        }
    })

    $("#genKeys").click(() => {
        var name = $("#nameRSA").val()
        if (name.length > 0) {
            alert("Choose the dir where the keys will be stored")
            operFolder().then((filename) => {
                modules.genKeys(filename, name)
                alert("RSA keys at: " + filename)
                $("#nameRSA").val("")
            })
        } else {
            alert("Name must be set")
        }
    })
})

function allHiden() {
    $("#secAES").hide()
    $("#secAuth").hide()
    $("#allProcess").hide()
    $("#CONFINT").hide()
}

function setDanger() {
    $("#SetPublicKey").addClass('is-danger')
    $("#SetFile").addClass('is-danger')
    $("#SetDestFolder").addClass('is-danger')
    $("#SetPrivateKey").addClass('is-danger')
}

function saveFile(file, type) {
    return new Promise((resolve) => {
        dialog.showSaveDialog({
            defaultPath: path.basename(file).split(".")[0] + type + path.extname(file)
        }, (filename) => {
            if (filename !== undefined) {
                resolve(filename)
            } else {
                alert("File name not set")
            }
        })
    })
}

function saveFilepath(file, type) {
    return path.dirname(file) + "/" + path.basename(file).split(".")[0] + type + path.extname(file)
}

function openFile() {
    return new Promise((resolve) => {
        dialog.showOpenDialog({
            properties: ['openFile']
        }, (filePaths) => {
            if (filePaths !== undefined) {
                resolve(filePaths[0])
            } else {
                alert("No file choosen")
            }
        })
    })
}

function operFolder() {
    return new Promise((resolve) => {
        dialog.showOpenDialog({
            properties: ['openDirectory']
        }, (filePaths) => {
            if (filePaths !== undefined) {
                resolve(filePaths[0])
            } else {
                alert("No folder choosen")
            }
        })
    })
}

function saveKey(filepath, key, filedest, iv) {
    return new Promise((resolve) => {
        console.log(filedest)
        if (key.length >= 8) {
            modules.SaveKey(key, filepath, filedest, iv)
            resolve(1)
        }
    })
}

function openkey() {
    return new Promise((resolve) => {
        alert("Choose private key")
        openFile().then((filekeypath) => {
            alert("Choose key")
            openFile().then((filepath) => {
                console.log(filepath, filekeypath)
                var key = modules.DecKey(filepath, filekeypath)
                if (key.length >= 8) {
                    resolve(key)
                }
            })
        })
    })
}

function ENCDEC(iv) {
    return new Promise((resolve) => {
        alert("Choose your file")
        openFile().then((filepath) => {
            if ($("#OPT-CIPHER").prop("checked")) {
                algo = "_AES_"
            } else {
                algo = "_DES_"
            }
            if ($("#AES-OPT").prop("checked")) {
                tipo = "_ENC_"
            } else {
                tipo = "_DEC_"
            }
            alert("Set file name")
            saveFile(filepath, algo + tipo).then((filedest) => {
                if (algo === "_AES_") {
                    if (tipo === "_ENC_") {
                        modules.AESCipher(filepath, filedest, $("#key").val(), iv, 1, 1)
                        resolve(1)
                    } else {
                        modules.AESCipher(filepath, filedest, $("#key").val(), iv, 1, 0)
                        resolve(1)
                    }
                } else {
                    if (tipo === "_ENC_") {
                        modules.AESCipher(filepath, filedest, $("#key").val(), iv, 0, 1)
                        resolve(1)
                    } else {
                        modules.AESCipher(filepath, filedest, $("#key").val(), iv, 0, 0)
                        resolve(1)
                    }
                }
            })
        })
    })
}

function makeid(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < length; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}