var keypair = require('keypair')
var cmd=require('node-cmd');
var fs = require('fs')
const NodeRSA = require('node-rsa');
var CryptoJS = require("crypto-js");
var sha1 = require('sha1');
const delim_hash = "__HASH__"
const delim_sign = "__SIGN__"
var genKeys = function (path, name) {
    var pair = keypair({
        bits: 1024
    });
    fs.writeFile(path + "/Public_key_" + name + ".pem", Buffer.from(pair.public), () => {
        fs.writeFile(path + "/Private_key_" + name + ".pem", Buffer.from(pair.private), () => {
            alert("RSA keys generated")
        })
    })
}

var shaFile = function (path, pathdest) {
    var hash = sha1(fs.readFileSync(path).toString())
    fs.writeFile(pathdest, fs.readFileSync(path) + delim_hash + hash, () => {
        alert("File with hash saved")
    })
}

var shaVerify = function (path) {
    return new Promise((resolve) => {
        var result = 0
        var file = fs.readFileSync(path).toString()
        content = file.split(delim_hash)
        if (sha1(content[0]) === (content[1])) {
            result = 1
            resolve(result)
        }
        resolve(result)
    })
}

var AESCipher = function (path, pathfinal, key,iv, algo,modo) {
    console.log( 'py ./script.py '+path+" "+pathfinal+" "+key+" "+iv+" "+algo+" "+modo)
    cmd.get(
        'py ./script.py '+path+" "+pathfinal+" "+key+" "+iv+" "+algo+" "+modo,
        function(err, data, stderr){
            alert("Done")
        }
    )
}

var RSACipher = function (text, keypath, opt) {
    var keyRSA = fs.readFileSync(keypath).toString()
    if (opt) {
        const key = new NodeRSA(keyRSA, "pkcs1-public")
        return key.encrypt(text, 'base64');
    } else {
        const key = new NodeRSA(keyRSA, "pkcs1-private")
        return key.decrypt(text, 'utf8');
    }
}

var RSASign = function (text, signature, keypath, opt) {
    var keyRSA = fs.readFileSync(keypath).toString()
    if (opt) {
        const key = new NodeRSA(keyRSA, "pkcs1-private")
        return key.sign(text, 'base64', "utf-8");
    } else {
        const key = new NodeRSA(keyRSA, "pkcs1-public")
        return key.verify(text, signature, 'utf-8', "base64");
    }
}

var SignatureFile = function (filepath, pathfinal, keypath) {
    var content = fs.readFileSync(filepath).toString()
    rsahash = RSASign(content, "", keypath, 1)
    fs.writeFile(pathfinal, content + delim_sign + rsahash, () => {
        //alert("File signed")
    })
}

var SignVerify = function (keypath, filepath) {
    var content = fs.readFileSync(filepath).toString().split(delim_sign)
    var original = content[0]
    var signature = content[1]
    console.log(original,signature)
    return RSASign(original, signature, keypath, 0)
}

var SaveKey = function (key, keypath, pathfinal,iv) {
    key = RSACipher(key+"|"+iv, keypath, 1)
    fs.writeFile(pathfinal, key, () => {
        //alert("Key saved")
    })
}

var DecKey = function (keypath, rsakeypath) {
    var content = fs.readFileSync(keypath).toString()
    key = RSACipher(content, rsakeypath, 0)
    return key
}

module.exports = {
    genKeys,
    AESCipher,
    RSACipher,
    shaFile,
    shaVerify,
    SignatureFile,
    SignVerify,
    RSASign,
    SaveKey,
    DecKey
}